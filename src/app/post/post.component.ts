import {Component, OnInit} from '@angular/core';
import {NgbDateStruct} from '@ng-bootstrap/ng-bootstrap';

import { HttpClient } from '@angular/common/http';

class Post {
  constructor(
    public title: string = '',
    public body: string = '',
  ) {
  }
}

@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.css']
})
export class PostComponent implements OnInit {

  apiRoot : string = "https://jsonplaceholder.typicode.com";

  // It maintains list of Posts
  posts = [];
  // It maintains post Model
  postModel: Post;
  // It maintains post form display status. By default it will be false.
  showNew: Boolean = false;
  // It will be either 'Save' or 'Update' based on operation.
  submitType: String = 'Save';
  // It maintains table row index based on selection.
  selectedRow: number;

  constructor(private http: HttpClient) {
    console.log(this.getPosts());
  }

  ngOnInit() {

  }

  getPosts() {
    console.log("GET");
    let url = `${this.apiRoot}/posts`;
    this.http.get(url).subscribe((res : any[])=>{
      console.log(res);
      this.posts = res;
    });
  }

  // This method associate to New Button.
  onNew() {
    // Initiate new post.
    this.postModel = new Post();
    // Change submitType to 'Save'.
    this.submitType = 'Save';
    // display post entry section.
    this.showNew = true;
  }

  // This method associate to Save Button.
  onSave() {
    if (this.submitType === 'Save') {
      // Push post model object into post list.
      this.posts.push(this.postModel);
    } else {
      // Update the existing properties values based on model.
      this.posts[this.selectedRow].title = this.postModel.title;
      this.posts[this.selectedRow].body = this.postModel.body;
    }
    // Hide post entry section.
    this.showNew = false;
  }

  // This method associate to Edit Button.
  onEdit(index: number) {
    // Assign selected table row index.
    this.selectedRow = index;
    // Initiate new post.
    this.postModel = new Post();
    // Retrieve selected post from list and assign to model.
    this.postModel = Object.assign({}, this.posts[this.selectedRow]);
    // Change submitType to Update.
    this.submitType = 'Update';
    // Display post entry section.
    this.showNew = true;
  }

  // This method associate to Delete Button.
  onDelete(index: number) {
    // Delete the corresponding post entry from the list.
    this.posts.splice(index, 1);
  }

  // This method associate toCancel Button.
  onCancel() {
    // Hide post entry section.
    this.showNew = false;
  }


}
